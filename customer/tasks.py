from datetime import datetime
import os
from django.core.mail import EmailMessage

import xlsxwriter
from customer.models import FileParser
from report import settings
from report.celery import app


@app.task()
def object_filter(filter_parameters):

    if filter_parameters['filter_by'] == 'email':
        fileparser_object_list = FileParser.objects.filter(customer=filter_parameters['customer_id'],
                                                    sended=filter_parameters['send_status'],
                                                    email_date__gte=filter_parameters['initial_date'],
                                                    email_date__lte=filter_parameters['finished_date'])

    else:
        fileparser_object_list = FileParser.objects.filter(customer=filter_parameters['customer_id'],
                                                    sended=filter_parameters['send_status'],
                                                    email_date__gte=filter_parameters['initial_date'],
                                                    email_date__lte=filter_parameters['finished_date'])


    now = datetime.now()
    directory = settings.MEDIA_ROOT + '/'#excel/'
    # os.path.join()
    # if os.path.exists(directory):

    try:
        os.stat(directory)
    except:
        os.mkdir(directory)

    xlsx_name = now.strftime("%m-%d-%Y,%H:%M:%S")
    workbook = xlsxwriter.Workbook(directory + xlsx_name + '.xlsx')
    worksheet = workbook.add_worksheet()

    worksheet.write('A1', 'CUSTOMER')
    worksheet.write('B1', 'EMAIL ID')
    worksheet.write('C1', 'EMAIL DATE')
    worksheet.write('D1', 'OPTION')
    worksheet.write('E1', 'SENDED')
    worksheet.write('F1', 'CREATE AT')
    row = 2

    for fileparser in fileparser_object_list:
        worksheet.write('A' + str(row), str(fileparser.customer.name))
        worksheet.write('B' + str(row), str(fileparser.email_id))
        worksheet.write('C' + str(row), str(fileparser.email_date))
        worksheet.write('D' + str(row), str(fileparser.option))
        worksheet.write('E' + str(row), str(fileparser.sended))
        worksheet.write('F' + str(row), str(fileparser.created_at))
        row = row + 1

    workbook.close()

    msg = EmailMessage('customers', '', 'osalas@lsv-tech.com', [filter_parameters['email']])
    msg.content_subtype = ""
    msg.attach_file(directory + xlsx_name + '.xlsx')
    msg.send()

    return 'Email Sent'













