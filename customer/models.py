from django.db import models
from customer import choices


class Customer (models.Model):

    name = models.CharField(max_length=30)
    parser = models.CharField(max_length=1, choices=choices.PARSER, default='a')

    def __str__(self):
        return self.name


class FileParser(models.Model):

    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    email_id = models.IntegerField(default=0)
    email_date = models.DateTimeField()
    option = models.CharField(max_length=5, choices=choices.OPTION, default='email')
    sended = models.BooleanField(default=False)
    file = models.FileField(upload_to='uploads/')
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.customer.name



