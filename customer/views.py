from django.http import HttpResponse
from django.shortcuts import render
from django.views import View
from customer.models import Customer
from customer.tasks import object_filter


def filter_view(request):
    return render(request, 'report/filter.html')


class FilterView(View):

    def get(self ,request):
        return render(request, 'report/filter.html',{'customers': Customer.objects.all()})

    def post(self, request):
        filter = {'filter_by': request.POST['filter_by'], 'customer_id': request.POST['customer'],
                  'initial_date': request.POST['initial_date'], 'finished_date': request.POST['finished_date'],
                  'send_status': request.POST['send_status'], 'email': request.user.email}

        object_filter.delay(filter)
        return render(request, 'report/filter.html',{'customers': Customer.objects.all()})










