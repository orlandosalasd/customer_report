from django.contrib import admin
from customer.models import Customer, FileParser

admin.site.register(Customer)
admin.site.register(FileParser)
