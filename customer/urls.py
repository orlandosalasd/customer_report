from django.contrib import admin
from django.urls import path, include

from customer.views import filter_view, FilterView

urlpatterns = [
    #path('filter/', filter_view, name='filter'),
    path('filter/', FilterView.as_view(), name='filter'),
]
